# Setup SSH IP Tunnel
# Usage
# =====
# ./tunnel.sh enable $fqdn
# ./tunnel.sh up $fqdn $mynet
# ./tunnel.sh down $fqdn
# ./tunnel.sh status


if [ $2 ]; then fqdn=$2; fi
if [ $3 ]; then mynet=$3; fi

if [ $1 = 'up' ]
then
echo 'up' 
echo $fqdn
echo $mynet
ssh $fqdn -w 0:0 bash -s < start.sh $mynet &
echo $! > tunnel.pid
sleep 7
./start_local.sh
elif [ $1 = 'down' ]
then
echo 'down'
./stop.sh $fqdn
elif [ $1 = 'enable' ]
then
echo "enable"
ssh $fqdn bash -s < setup.sh
elif [ $1 = 'status' ]
then
echo "checking status"
if [ -e tunnel.pid ]; then echo "Tunnel is up"; else echo "No Tunnels found"; fi 
else
cat <<EOL
Usage
=====
./tunnel.sh enable \$fqdn
./tunnel.sh up \$fqdn \$mynet
./tunnel.sh down \$fqdn
./tunnel.sh status
EOL
fi
