# Usage
# =====
# source ./stop.sh 
# ./stop.sh $fqdn
#job=$(jobs | grep "ssh $fqdn" | awk '{print $1}' | sed 's/[^0-9]*//g')
#kill %$job
#wait %$job
fqdn=$1
pid=$(cat tunnel.pid)
kill -9 $pid
sleep 7
rm tunnel.pid
ssh $fqdn 'systemctl restart network NetworkManager'
