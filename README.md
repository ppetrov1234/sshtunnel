sshtunnel - share your internet access over ssh ip tunnnel
==========================================================

Description
-----------

**sshtunnel** allows you to build ssh ip tunnel and in case your PC has internet connectivity you will give internet connectivity to the remote host.

Usage
-----

Clone the repo

```
git clone https://gitlab.com/ppetrov1234/sshtunnel.git
cd sshtunnel
```

Specify the network which should be routable around the VPN (over normal current default gateway of the remote host)

```
fqdn=10.101.76.118
mynet=10.229.0.0/16
```

Make sure you have root access to local and remote hosts and able to login over ssh. It is a good idea to copy your ssh public key to the remote host by means of ssh-copy-id

```
ssh-copy-id $fqdn
```

sshtunnel commands:
- enable $fqdn : configure sshd to allow ssh ip tunnels
- up $fqdn $mynet : establish ssh ip tunnel
- down $fqdn : stop the tunnel
- status : check if ssh tunnel is currently active

```
./tunnel.sh enable $fqdn
./tunnel.sh up $fqdn $mynet
./tunnel.sh down $fqdn
./tunnel.sh status
```
