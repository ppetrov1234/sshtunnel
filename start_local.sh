# Usage
# =====
# bash start_local.sh

ip link set tun0 up
ip addr add 10.0.0.1/30 dev tun0
iptables -t nat -A POSTROUTING -s 10.0.0.2 -j MASQUERADE
sysctl -w net.ipv4.ip_forward=1

