# Usage
# =====
# ssh $fqdn bash -s < setup.sh

grep "PermitTunnel yes" /etc/ssh/sshd_config
if [ $? -eq 0 ]
then
  printf "Already done\n"
else
  sed -i "s/#PermitTunnel no/PermitTunnel yes/" /etc/ssh/sshd_config; \
  systemctl restart sshd
  printf "Setup succesfull\n"
fi
