# Usage
# =====
# ssh $fqdn -w 0:0 bash -s < start.sh $mynet &

#mynet=10.229.0.0/16
mynet=$1
rgw=$(ip r | grep default | awk 'NR==1{print $3}')
ip addr add 10.0.0.2/30 dev tun0
ip route add $mynet via $rgw 
#ip route add 10.229.0.0/16 via 10.101.76.1 
ip link set tun0 up
sleep 5
ip route del default
ip route add default via 10.0.0.1
ping 10.0.0.1 -c 10
ping 10.0.0.1 > /dev/null

